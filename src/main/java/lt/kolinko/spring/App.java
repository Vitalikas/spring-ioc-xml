package lt.kolinko.spring;

import lt.kolinko.spring.services.MusicPlayer;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("xml/appContext.xml");

//        Music music = context.getBean("musicBean", Music.class);
//
//        MusicPlayer musicPlayer = new MusicPlayer(music);

        MusicPlayer musicPlayer = context.getBean("musicPlayerBean", MusicPlayer.class);

        musicPlayer.playMusic();

        System.out.println("Name: " + musicPlayer.getName());
        System.out.println("Volume: " + musicPlayer.getVolume() + "%");

        /*
        testing scope-prototype
        use <bean ... scope="prototype">

        MusicPlayer musicPlayer1 = context.getBean("musicPlayerBean", MusicPlayer.class);
        boolean comp = musicPlayer == musicPlayer1;
        System.out.println(comp);
        System.out.println(musicPlayer);
        System.out.println(musicPlayer1);

        musicPlayer.setVolume(41.7);
        System.out.println(musicPlayer.getVolume());
        System.out.println(musicPlayer1.getVolume());
         */

        context.close();
    }
}
