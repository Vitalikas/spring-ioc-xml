package lt.kolinko.spring.services;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lt.kolinko.spring.repositories.Music;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MusicPlayer {
    private List<Music> musicList;
    private String name;
    private double volume;

    public void playMusic() {
        musicList.forEach(music -> System.out.println("Playing: " + music.getSong()));
    }

    public void init() {
        System.out.println("Initialization...");
    }

    public void destroy() {
        System.out.println("Destroying...");
    }
}
