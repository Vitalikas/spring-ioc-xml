package lt.kolinko.spring.repositories;

public class PopMusic implements Music {
    private PopMusic() {
    }

    public static PopMusic getPopMusic() {
        return new PopMusic();
    }

    @Override
    public String getSong() {
        return "Ariana Grande - No Tears Left To Cry";
    }
}
